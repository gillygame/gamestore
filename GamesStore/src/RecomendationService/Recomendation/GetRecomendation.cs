﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecomendationService.Recomendation
{
    public class Transaction
    {
        public double Support { get; set; }
        public double Confident { get; set; }
        public HashSet<int> TransactionOperation { get; set; }
    }

    public class GetRecomendation
    {
        public int[] Recomendation(List<int[]> item, int elementRecomendation)
        {
            var needItem = item.Where(x => x.Contains(elementRecomendation)).ToList();
            var supportItem = needItem.Count / (double)item.Count;

            var itemsetList = GetSet(needItem);
            var supportValue = 2 / (double)item.Count;

            var generator = new AprioriFrequentItemsetGenerator<int>();
            var data = generator.generate(itemsetList, supportValue, item.Count);
            var collection = data.getFrequentItemsetList();
            var combinationSupport = new List<Transaction>();

            foreach (HashSet<int> itemset in collection)
            {
                var support = data.getSupport(itemset);
                if (support <= supportValue)
                    continue;
                combinationSupport.Add(new Transaction
                {
                    Support = support,
                    TransactionOperation = itemset,
                    Confident = support / supportItem
                });
            }

            var recomendationTransaction = GetMostConfidentTransaction(combinationSupport);

            return recomendationTransaction.TransactionOperation.Where(x => x != elementRecomendation).ToArray();
        }

        private List<HashSet<int>> GetSet(List<int[]> item)
        {
            var collection = new List<HashSet<int>>();
            foreach (var element in item)
            {
                var hash = new HashSet<int>();
                foreach (var set in element)
                {
                    hash.Add(set);
                }
                collection.Add(hash);
            }
            return collection;
        }

        private Transaction GetMostConfidentTransaction(List<Transaction> collection)
        {
            return collection
                .OrderByDescending(x => x.Confident)
                .FirstOrDefault();
        }

    }


}

