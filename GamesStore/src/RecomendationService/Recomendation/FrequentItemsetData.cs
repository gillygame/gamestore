﻿using System.Collections.Generic;
using System.Linq;

namespace RecomendationService.Recomendation
{
    public class FrequentItemsetData<I>
    {

        public List<HashSet<I>> frequentItemsetList;
        public Dictionary<HashSet<I>, int> supportCountMap;
        public double minimumSupport;
        public int numberOfTransactions;
        public int Count;

        public FrequentItemsetData(List<HashSet<I>> frequentItemsetList,
                            Dictionary<HashSet<I>, int> supportCountMap,
                            double minimumSupport,
                            int transactionNumber)
        {
            this.frequentItemsetList = frequentItemsetList;
            this.supportCountMap = supportCountMap;
            this.minimumSupport = minimumSupport;
            this.numberOfTransactions = transactionNumber;
        }



        public List<HashSet<I>> getFrequentItemsetList()
        {
            return frequentItemsetList;
        }

        public Dictionary<HashSet<I>, int> getSupportCountMap()
        {
            return supportCountMap;
        }

        public double getMinimumSupport()
        {
            return minimumSupport;
        }

        public int getTransactionNumber()
        {
            return numberOfTransactions;
        }

        public double getSupport(HashSet<I> index)
        {
            if (supportCountMap.ContainsKey(index))
                return 1.0 * supportCountMap[index] / numberOfTransactions;
            return 0;
        }
    }
}
