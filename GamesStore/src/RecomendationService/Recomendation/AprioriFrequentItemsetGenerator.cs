﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RecomendationService.Recomendation
{
    public class AprioriFrequentItemsetGenerator<I>
    {

        public FrequentItemsetData<I> generate(List<HashSet<I>> transactionList,
                                                 double minimumSupport, int count)
        {
            checkSupport(minimumSupport);

            if (transactionList.Count == 0)
            {
                return null;
            }

            var supportCountMap = new Dictionary<HashSet<I>, int>();

            List<HashSet<I>> frequentItemList = findFrequentItems(transactionList,
                                                               supportCountMap,
                                                               minimumSupport);

            var map = new Dictionary<int, List<HashSet<I>>>();
            map.Add(1, frequentItemList);
            int k = 1;

            do
            {
                ++k;

                List<HashSet<I>> candidateList =
                        generateCandidates(map[k - 1]);

                foreach (HashSet<I> transaction in transactionList)
                {
                    List<HashSet<I>> candidateList2 = subset(candidateList,
                                                         transaction);

                    foreach (HashSet<I> itemset in candidateList2)
                    {
                        var value = 0;
                        if (supportCountMap.TryGetValue(itemset, out value))
                        {
                            supportCountMap[itemset]++;
                        }
                        else if (!supportCountMap.ContainsKey(itemset))
                        {
                            supportCountMap.Add(itemset, 1);
                        }
                    }
                }

                map.Add(k, getNextItemsets(candidateList,
                                           supportCountMap,
                                           minimumSupport,
                                          count));

            } while (map[k].Count != 0);

            return new FrequentItemsetData<I>(extractFrequentItemsets(map),
                                             supportCountMap,
                                             minimumSupport,
                                             count);
        }

        private List<HashSet<I>>
            extractFrequentItemsets(Dictionary<int, List<HashSet<I>>> map)
        {
            var ret = new List<HashSet<I>>();

            foreach (List<HashSet<I>> itemsetList in map.Values)
            {
                ret.AddRange(itemsetList);
            }

            return ret;
        }


        private List<HashSet<I>> getNextItemsets(List<HashSet<I>> candidateList,
                                             Dictionary<HashSet<I>, int> supportCountMap,
                                             double minimumSupport,
                                             int transactions)
        {
            var ret = new List<HashSet<I>>(candidateList.Count);

            foreach (HashSet<I> itemset in candidateList)
            {
                if (supportCountMap.ContainsKey(itemset))
                {
                    int supportCount = supportCountMap[itemset];
                    double support = 1.0 * supportCount / transactions;

                    if (support >= minimumSupport)
                    {
                        ret.Add(itemset);
                    }
                }
            }

            return ret;
        }

        private List<HashSet<I>> subset(List<HashSet<I>> candidateList,
                                   HashSet<I> transaction)
        {
            var ret = new List<HashSet<I>>(candidateList.Count);

            foreach (HashSet<I> candidate in candidateList)
            {

                if (ContainAll(candidate, transaction))
                {
                    ret.Add(candidate);
                }
            }

            return ret;
        }
        private static bool ContainAll(HashSet<I> candidate, HashSet<I> transaction)
        {
            foreach (var item in candidate)
            {
                if (!transaction.Contains(item))
                {
                    return false;
                }
            }
            return true;
        }

        private List<HashSet<I>> generateCandidates(List<HashSet<I>> itemsetList)
        {
            var list = new List<List<I>>(itemsetList.Count);

            foreach (HashSet<I> itemset in itemsetList)
            {
                List<I> l = new List<I>(itemset);
                l.Sort();
                list.Add(l);
            }

            int listSize = list.Count;

            var ret = new List<HashSet<I>>(listSize);

            for (int i = 0; i < listSize; ++i)
            {
                for (int j = i + 1; j < listSize; ++j)
                {
                    HashSet<I> candidate = tryMergeItemsets(list[i], list[j]);

                    if (candidate != null)
                    {
                        ret.Add(candidate);
                    }
                }
            }

            return ret;
        }

        private HashSet<I> tryMergeItemsets(List<I> itemset1, List<I> itemset2)
        {
            int length = itemset1.Count;

            for (int i = 0; i < length - 1; ++i)
            {
                if (!itemset1[i].Equals(itemset2[i]))
                {
                    return null;
                }
            }

            if (itemset1[length - 1].Equals(itemset2[length - 1]))
            {
                return null;
            }

            var ret = new HashSet<I>();

            for (int i = 0; i < length - 1; ++i)
            {
                ret.Add(itemset1[i]);
            }

            ret.Add(itemset1[length - 1]);
            ret.Add(itemset2[length - 1]);
            return ret;
        }


        private List<HashSet<I>> findFrequentItems(List<HashSet<I>> itemsetList,
                                          Dictionary<HashSet<I>, int> supportCountMap,
                                          double minimumSupport)
        {
            Dictionary<I, int> map = new Dictionary<I, int>();

            // Count the support counts of each item.
            foreach (HashSet<I> itemset in itemsetList)
            {
                foreach (I item in itemset)
                {
                    HashSet<I> tmp = new HashSet<I>();
                    tmp.Add(item);

                    if (supportCountMap.Keys.Any(x => x.Contains(item)))
                    {
                        var key = supportCountMap.Keys.FirstOrDefault(x => x.Contains(item));
                        supportCountMap[key]++;
                    }
                    else
                    {
                        supportCountMap.Add(tmp, 1);
                    }
                    int value = 0;
                    if (map.TryGetValue(item, out value))
                        map[item]++;
                    else if (!map.ContainsKey(item))
                        map.Add(item, 0 + 1);
                }
            }

            var frequentItemsetList = new List<HashSet<I>>();

            foreach (KeyValuePair<I, int> entry in map)
            {
                if (1.0 * entry.Value / map.Count >= minimumSupport)
                {
                    HashSet<I> itemset = new HashSet<I>();
                    itemset.Add(entry.Key);
                    frequentItemsetList.Add(itemset);
                }
            }

            return frequentItemsetList;
        }

        private void checkSupport(double support)
        {

            if (support > 1.0)
            {
                throw new Exception(
                        "The input support is too large: " + support + ", " +
                        "should be at most 1.0");
            }

            if (support < 0.0)
            {
                throw new Exception(
                        "The input support is too small: " + support + ", " +
                        "should be at least 0.0");
            }
        }
    };
}
