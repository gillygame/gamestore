﻿using GameStore.Repository.GameRepositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;



namespace GameStore.Repository.GameRepositories
{
    public class GameWriteRepository : IGameWriteRepository
    {

        private readonly DbSet<Game> gameList;
        private readonly DbContext contextDb;

        public GameWriteRepository(DbContext contextDb)
        {
            this.gameList = contextDb.Set<Game>();
            this.contextDb = contextDb;
        }
        public Task AddAsync(Game game)
        {
            gameList.Add(game);
            return contextDb.SaveChangesAsync();
        }

        public Task DeleteAsync(int gameId)
        {
            var temp = gameList.FirstOrDefault(x => x.GameId == gameId);
            gameList.Remove(temp);
            return contextDb.SaveChangesAsync();
        }

        public Task EditAsync(Game game)
        {
            var temp = gameList.FirstOrDefault(x => x.GameId == game.GameId);
            temp.Description = game.Description;
            temp.Name = game.Name;
            temp.Price = game.Price;
          
            contextDb.Entry(temp).State = EntityState.Modified;
            return contextDb.SaveChangesAsync();
        }
    }
}
