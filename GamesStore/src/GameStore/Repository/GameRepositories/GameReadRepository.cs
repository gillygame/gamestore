﻿using GameStore.Repository.GameRepositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;
using GameStore.Repository.CategoryRepositories.Interfaces;

namespace GameStore.Repository.GameRepositories
{
    public class GameReadRepository : IGameReadRepository
    {


        private readonly DbSet<Game> gameList;
        private readonly DbSet<Category> categoryList;
        private readonly DbContext contextDb;
        private readonly ICategoryReadRepository categoryReadRepository;

        public GameReadRepository(DbContext contextDb, ICategoryReadRepository readCategory)
        {
            this.gameList = contextDb.Set<Game>();
            this.categoryList = contextDb.Set<Category>();
            this.contextDb = contextDb;
            this.categoryReadRepository = readCategory;
        }

        public Task<Game[]> GetAllGameAsync()
        {
            return gameList.Include(x => x.Category).ToArrayAsync();
        }

        public async Task<Game[]> GetGameByCategoryAsync(string categoryId)
        {
            return await gameList
                .Include(x=>x.Category)
                .Where(x => x.Category.Name == categoryId)
                .ToArrayAsync();
        }

        private Task<Category> GetCategoryAsync(int categoryId)
        {
            return categoryList
                .FirstOrDefaultAsync(x => x.CategoryId.Equals(categoryId));
        }

        public Task<Game> GetGameByIdAsync(int id)
        {
            return gameList
                .Include(x=> x.Category)
                .FirstOrDefaultAsync(x => x.GameId.Equals(id));
        }

        public Task<Game[]> GetGameByNameAsync(string name)
        {
            return gameList
                .Include(x => x.Category)
                .Where(x => x.Name.Contains(name))
                .ToArrayAsync();
        }
    }
}
