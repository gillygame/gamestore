﻿using GameStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Repository.GameRepositories.Interfaces
{
    public interface IGameReadRepository
    {
        Task<Game[]> GetGameByNameAsync(string name);
        Task<Game> GetGameByIdAsync(int id);
        Task<Game[]> GetGameByCategoryAsync(string category);
        Task<Game[]> GetAllGameAsync();
    }
}
