﻿using GameStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Repository.GameRepositories.Interfaces
{
    public interface IGameWriteRepository
    {
        Task AddAsync(Game game);
        Task DeleteAsync(int game);
        Task EditAsync(Game game);
    }
}
