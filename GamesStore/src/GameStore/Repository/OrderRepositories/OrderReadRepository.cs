﻿using GameStore.Repository.OrderRepositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace GameStore.Repository.OrderRepositories
{
    public class OrderReadRepository : IOrderReadRepository
    {

        private readonly DbSet<Order> orderList;
        private readonly DbContext contextDb;
        private readonly DbSet<OrderGames> orderGameList;

        public OrderReadRepository(DbContext contextDb)
        {
            orderList = contextDb.Set<Order>();
            orderGameList = contextDb.Set<OrderGames>();

            this.contextDb = contextDb;
        }
        public Task<Order[]> GetAllOrederAsync()
        {
            return orderList
                .Include(x => x.Customer)
                .ToArrayAsync();
        }

        public Task<Order[]> GetCustomerOrderAsync(int customerId)
        {
            return orderList
                .Include(x => x.Customer)
                .Where(x => x.Customer.CustomerID.Equals(customerId) && x.Status == true)
                .ToArrayAsync();
        }

        public Task<OrderGames[]> GetGameFromOrderAsync(int orderId)
        {
            return orderGameList
                .Include(x => x.Game)
                .Include(x => x.Order)
                .Include(operation => operation.Game.Category)
                .Where(x => x.Order.OrderId == orderId)
                .ToArrayAsync();

        }

        public Task<Order> GetLastUnCompleatCustomerOrderAsync(int customerId)
        {
            return orderList
                .Include(x=> x.OrderGame)
               .FirstOrDefaultAsync(order => order.CustomerId == customerId && order.Status == false);
        }



        public Task<Order> GetOrderByIdAsync(int orderId)
        {
            return orderList
                .Include(x => x.Customer)
                .Include(x => x.OrderGame)
                .FirstOrDefaultAsync(x => x.OrderId == orderId);
        }

        public Task<OrderGames[]> GetCustomerGameAsync(int customerId)
        {
            return orderGameList
                .Include(x => x.Game)
                .Include(x => x.Order)
                .Include(operation => operation.Game.Category).
                Where(x => x.Order.CustomerId == customerId).ToArrayAsync();

        }

        public Task<OrderGames[]> GetGameFromBucketAsync(Order order)
        {
            return orderGameList
                .Include(x => x.Game)
                .Include(x => x.Order)
                .Include(operation => operation.Game.Category)
                .Where(x => x.Order.OrderId == order.OrderId)
                .ToArrayAsync();
        }

        public Task<Order[]> GetAllOrder()
        {
            return orderList
                .Include(x => x.OrderGame)
                .Where(x => x.Status)
                .ToArrayAsync();
        }
    }
}
