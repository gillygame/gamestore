﻿using GameStore.Repository.OrderRepositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;

namespace GameStore.Repository.OrderRepositories
{
    public class OrderWriteRepository : IOrderWriteRepository
    {

        private readonly DbSet<Order> orderList;
        private readonly DbSet<OrderGames> orderGameList;
        private readonly DbContext contextDb;

        public OrderWriteRepository(DbContext contextDb)
        {
            orderList = contextDb.Set<Order>();
            orderGameList = contextDb.Set<OrderGames>();
            this.contextDb = contextDb;
        }
        public Task AddOrderAsync(Order order)
        {
            orderList.Add(order);
            return contextDb.SaveChangesAsync();
        }

        public Task UpdateOrderAsync(Order order)
        {
            var temp = orderList.FirstOrDefault(x => x.OrderId == order.OrderId);
            temp.Status = true;
            contextDb.Entry(temp).State = EntityState.Modified;
            return contextDb.SaveChangesAsync();
        }

        public Task AddGameToBucketAsync(OrderGames game)
        {
           orderGameList.Add(game);
            return contextDb.SaveChangesAsync();
        }

        public Task DeleteFromBucketAsync(OrderGames game)
        {
            var searchGame = orderGameList.FirstOrDefault
                (x => x.GameId.Equals(game.GameId) && x.OrderId.Equals(game.OrderId));
            orderGameList.Remove(searchGame);
            return contextDb.SaveChangesAsync();
        }

        public Task DeleteOrderAsync(Order order)
        {
            var deleteOrder = orderList
                .Include(x => x.OrderGame)
                .FirstOrDefault(x => x.OrderId == order.OrderId);
            orderGameList.RemoveRange(deleteOrder.OrderGame);
            orderList.RemoveRange(deleteOrder);
            return contextDb.SaveChangesAsync();
        }
    }
}
