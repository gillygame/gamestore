﻿using GameStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Repository.OrderRepositories.Interfaces
{
    public interface IOrderWriteRepository
    {
        Task AddOrderAsync(Order order);
        Task UpdateOrderAsync(Order order);
        Task DeleteOrderAsync(Order order);
        Task AddGameToBucketAsync(OrderGames game);
        Task DeleteFromBucketAsync(OrderGames game);
    }
}
