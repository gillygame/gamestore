﻿using GameStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Repository.OrderRepositories.Interfaces
{
    public interface IOrderReadRepository
    {
        Task<Order> GetOrderByIdAsync(int orderId);
        Task<Order[]> GetAllOrder();
        Task<Order[]> GetCustomerOrderAsync(int customerId);
        Task<Order> GetLastUnCompleatCustomerOrderAsync(int customerId);
        Task<Order[]> GetAllOrederAsync();
        Task<OrderGames[]> GetGameFromBucketAsync(Order order);
        Task<OrderGames[]> GetCustomerGameAsync(int order);
        Task<OrderGames[]> GetGameFromOrderAsync(int orderId);
    }
}
