﻿using GameStore.Repository.RateRepository.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace GameStore.Repository.RateRepository
{
    public class RateReadRepository : IRateReadRepository
    {
        private readonly DbSet<Rate> rateList;
        private readonly DbSet<OrderGames> orderGamesList;
        private readonly DbSet<Game> gameList;
        private readonly DbContext contextDb;

        public RateReadRepository(DbContext contextDb)
        {
            rateList = contextDb.Set<Rate>();
            gameList = contextDb.Set<Game>();
            orderGamesList = contextDb.Set<OrderGames>();
            this.contextDb = contextDb;
        }

        public Task<Rate[]> GetGameRates(int gameId)
        {
            return rateList
                .Include(x => x.Customer)
                .Include(x => x.Game)
                .Where(x => x.GameId == gameId)
                .ToArrayAsync();
        }

        public Task<int[]> GetMostSaleGame()
        {
            return orderGamesList
                   .Include(x => x.Game)
                   .GroupBy(x => x.GameId)
                   .OrderByDescending(x => x.Count())
                   .Select(x => x.Key)
                   .Take(5)
                   .ToArrayAsync();
        }

        public Task<int[]> GetMostSaleGameByCategory(int categoryId)
        {
            return orderGamesList
                 .Include(x => x.Game)
                 .Where(x => x.Game.CategoryId.Equals(categoryId))
                 .GroupBy(x => x.GameId)
                 .OrderByDescending(x => x.Count())
                 .Select(x => x.Key)
                 .Take(5)
                 .ToArrayAsync();
        }

        public Task<int[]> GetMostRateGame()
        {
            return rateList
                .Include(x => x.Game)
                .GroupBy(x => x.GameId)
                .Select(m => new Rate { GameId = m.Key, CustomerRate = m.Average(v => v.CustomerRate) })
                .OrderByDescending(m => m.CustomerRate)
                .Select(m => m.GameId)
                .Take(5)
                .ToArrayAsync();
        }
    }
}
