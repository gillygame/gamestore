﻿using GameStore.Repository.RateRepository.Interfaces;
using System.Threading.Tasks;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;

namespace GameStore.Repository.RateRepository
{
    public class RateWriteRepository : IRateWriteRepository
    {
        private readonly DbSet<Rate> rateList;
        private readonly DbContext contextDb;

        public RateWriteRepository(DbContext contextDb)
        {
            rateList = contextDb.Set<Rate>();
            this.contextDb = contextDb;
        }

        public Task AddFeedAsync(Rate rate)
        {
            rateList.Add(rate);
            return contextDb.SaveChangesAsync();
        }
    }
}
