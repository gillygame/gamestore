﻿using GameStore.Entities;
using System.Threading.Tasks;

namespace GameStore.Repository.RateRepository.Interfaces
{
    public interface IRateReadRepository
    {
        Task<Rate[]> GetGameRates(int gameId);
        Task<int[]> GetMostSaleGame();
        Task<int[]> GetMostRateGame();
        Task<int[]> GetMostSaleGameByCategory(int categoryId);
    }
}
