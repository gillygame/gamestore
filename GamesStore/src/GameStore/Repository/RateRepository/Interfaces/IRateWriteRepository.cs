﻿using GameStore.Entities;
using System.Threading.Tasks;

namespace GameStore.Repository.RateRepository.Interfaces
{
    public interface IRateWriteRepository
    {
        Task AddFeedAsync(Rate rate);
    }
}
