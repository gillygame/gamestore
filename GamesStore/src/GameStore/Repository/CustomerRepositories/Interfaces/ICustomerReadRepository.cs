﻿using GameStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Repository.CustomerRepositories.Interfaces
{
    public interface ICustomerReadRepository
    {
        Task GetCustomerByEmailAsync(string email);
        Task GetCustomerByIdAsync(int id);
        Task<Customer> GetUserByLoginAsync(string login);
        Task<Customer[]> GetAllCustomerAsync();
    }
}
