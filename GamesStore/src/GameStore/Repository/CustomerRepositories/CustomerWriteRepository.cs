﻿using GameStore.Repository.CustomerRepositories.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;

namespace GameStore.Repository.CustomerRepositories
{
    public class CustomerWriteRepository : ICustomerWriteRepository
    {
        private readonly DbSet<Customer> customerList;
        private readonly DbContext contextDb;

        public CustomerWriteRepository(DbContext context)
        {
            contextDb = context;
            this.customerList= contextDb.Set<Customer>();
        }
         
        public Task AddAsync(Customer customer)
        {
            customerList.Add(customer);

           return contextDb.SaveChangesAsync();
            
        }

        public Task DeleteAsync(Customer customer)
        {
            customerList.Remove(customer);
            return contextDb.SaveChangesAsync();
        }

        public Task UpdateAsync(Customer customer)
        {
            var temp = customerList.Where(x => x.CustomerID == customer.CustomerID).FirstOrDefault();
            temp = customer;
            contextDb.Entry(temp).State = EntityState.Modified;
            return contextDb.SaveChangesAsync();
        }
    }
}
