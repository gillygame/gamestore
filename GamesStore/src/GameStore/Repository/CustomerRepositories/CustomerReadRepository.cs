﻿using GameStore.Repository.CustomerRepositories.Interfaces;
using System.Threading.Tasks;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;

namespace GameStore.Repository.CustomerRepositories
{
    class CustomerReadRepository : ICustomerReadRepository
    {
        private readonly DbSet<Customer> customerList;

        public CustomerReadRepository(DbContext context)
        {
            customerList = context.Set<Customer>();
        }

        public Task<Customer[]> GetAllCustomerAsync()
        {
            return customerList
                .ToArrayAsync();
        }

        public Task GetCustomerByEmailAsync(string email)
        {
            return customerList
                .FirstOrDefaultAsync(x => x.Email.ToLower() == email.ToLower());
        }

        public Task GetCustomerByIdAsync(int id)
        {
            return customerList
                .FirstOrDefaultAsync(x => x.CustomerID == id);
        }

        public Task<Customer> GetUserByLoginAsync(string login)
        {
            return customerList
                .FirstOrDefaultAsync(x => x.Email.Split('@')[0] == login);
        }
    }
}
