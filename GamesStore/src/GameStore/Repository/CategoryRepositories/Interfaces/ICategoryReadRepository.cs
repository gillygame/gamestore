﻿using GameStore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Repository.CategoryRepositories.Interfaces
{
    public interface ICategoryReadRepository
    {
        Task<Category[]> GetAllCategoryAsync();

         int GetCategoryByName(string name);
    }
}
