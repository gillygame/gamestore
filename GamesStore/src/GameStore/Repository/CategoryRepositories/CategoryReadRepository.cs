﻿using System.Threading.Tasks;
using System.Linq;
using GameStore.Entities;
using GameStore.Repository.CategoryRepositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace GameStore.Repository.CategoryRepositories
{
    public class CategoryReadRepository: ICategoryReadRepository
    {
        public DbSet<Category> Category { get; set; }


        public CategoryReadRepository(DbContext context)
        {
            Category = context.Set<Category>();
        }

        public Task<Category[]> GetAllCategoryAsync()
        {
           return Category.ToArrayAsync();
        }

        public int GetCategoryByName(string name)
        {
            var category = Category
                .FirstOrDefault(x => x.Name.Equals(name));
            if (category == null)
                throw new System.Exception();
            return category.CategoryId;
        }
    }
}





