﻿(function () {
    'use strict';
    angular
        .module('GameStoreApp', ['toaster', 'ngAnimate']);
})();