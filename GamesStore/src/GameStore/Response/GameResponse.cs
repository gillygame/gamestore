﻿namespace GameStore.Response
{
    public class GameResponse
    {
        public string GameId { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string CategoryId { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public string Photo { get; set; }
    }
}
