﻿namespace GameStore.Response
{
    public class OrderResponse
    {
        public int OrderId { get; set; }
        public string Date { get; set; }
        public string Customer { get; set; }
        public GameResponse[] Games { get; set; }
    }
}
