﻿namespace GameStore.Response
{
    public class RateResponse
    {
        public int RateId { get; set; }
        public string Customer { get; set; }
        public string Comment { get; set; }
        public double Mark { get; set; }
    }
}
