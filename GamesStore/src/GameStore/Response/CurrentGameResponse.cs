﻿namespace GameStore.Response
{
    public class CurrentGameResponse
    {
        public int GameId { get; set; }
    }
}
