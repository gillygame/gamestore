﻿namespace GameStore.Request
{
    public class AccountRequest
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string DateBirthsday { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }
    }
}
