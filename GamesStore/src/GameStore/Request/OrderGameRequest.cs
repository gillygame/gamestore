﻿namespace GameStore.Request
{
    public class OrderGameRequest
    {
        public int GameId { get; set; }
        public string Customer { get; set; }
    }
}
