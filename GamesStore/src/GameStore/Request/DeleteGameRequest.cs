﻿namespace GameStore.Request
{
    public class DeleteGameRequest
    {
        public int OrderId { get; set; }
        public int GameId { get; set; }
    }
}
