﻿namespace GameStore.Request
{
    public class RateRequest
    {
        public string Customer { get; set; }
        public int GameId { get; set; }
        public string Comment { get; set; }
        public double CustomerRate { get; set; }
    }
}
