﻿using System.ComponentModel.DataAnnotations;

namespace GameStore.Request
{
    public class LoginViewRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}