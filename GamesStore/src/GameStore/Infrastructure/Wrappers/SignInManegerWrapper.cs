﻿using System.Security.Claims;
using GameStore.Authorization.Entities.Identity;
using Microsoft.AspNetCore.Identity;

namespace GameStore.Infrastructure.Wrappers
{
    public class SignInManegerWrapper
    {
        private readonly SignInManager<IdentityUser> signInManager;

        public SignInManegerWrapper(SignInManager<IdentityUser> signInManager)
        {
            this.signInManager = signInManager;
        }

        public bool IsSignedIn(ClaimsPrincipal user)
        {
            return signInManager.IsSignedIn(user);
        }
    }
}