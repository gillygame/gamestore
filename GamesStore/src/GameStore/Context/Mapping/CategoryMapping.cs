﻿using GameStore.Context.Interfaces;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.Context.Mapping
{
    public class CategoryMapping : IMappingInterface<Category>
    {
        public void MapEntity(EntityTypeBuilder<Category> builder)
        {
            builder.ForNpgsqlToTable("categories");
            builder.HasKey(entity => entity.CategoryId);
            builder.HasMany(entity => entity.Games);
            builder.Property(entity => entity.CategoryId).HasColumnName("categoryid");
            builder.Property(entity => entity.Name).HasColumnName("name");
        }
    }
}
