﻿using GameStore.Authorization.Entities.Identity;
using GameStore.Context.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.Context.Mapping
{
    public class IdentityUserRoleMapping : IMappingInterface<IdentityUserRole>
    {
        public void MapEntity(EntityTypeBuilder<IdentityUserRole> builder)
        {
            builder.ForNpgsqlToTable("users_roles", "identity");
            builder.HasKey(entity => new { entity.UserId, entity.RoleId });
            builder.Property(entity => entity.UserId).HasColumnName("user_id").IsRequired();
            builder.Property(entity => entity.RoleId).HasColumnName("role_id").IsRequired();
        }
    }
}
