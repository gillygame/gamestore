﻿using System;
using GameStore.Context.Interfaces;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.Context.Mapping
{
    public class RateMapping : IMappingInterface<Rate>
    {
        public void MapEntity(EntityTypeBuilder<Rate> builder)
        {
            builder.ForNpgsqlToTable("rates");
            builder.HasKey(entity => entity.RateId);
            builder.HasOne(entity => entity.Customer).WithMany(x => x.UsersRate);
            builder.HasOne(entity => entity.Game).WithMany(x => x.GamesRate);

            builder.Property(entity => entity.RateId).HasColumnName("ratesid");
            builder.Property(entity => entity.CustomerId).HasColumnName("customer");
            builder.Property(entity => entity.GameId).HasColumnName("game");
            builder.Property(entity => entity.Comment).HasColumnName("comment");
            builder.Property(entity => entity.CustomerRate).HasColumnName("customer_rate");
        }
    }
}
