﻿using GameStore.Context.Interfaces;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.Context.Mapping
{
    public class CustomerMapping : IMappingInterface<Customer>
    {
        public void MapEntity(EntityTypeBuilder<Customer> builder)
        {
            builder.ForNpgsqlToTable("customers");
            builder.HasKey(entity => entity.CustomerID);

            builder.Property(entity => entity.CustomerID).HasColumnName("customerid");
            builder.Property(entity => entity.FullName).HasColumnName("fullname");
            builder.Property(entity => entity.Email).HasColumnName("email");
            builder.Property(entity => entity.Password).HasColumnName("password");
            builder.Property(entity => entity.DateBirthsday).HasColumnName("date_birthsday");
            builder.Property(entity => entity.DateRegistration).HasColumnName("date_registration");
            builder.Property(entity => entity.Country).HasColumnName("country");
            builder.Property(entity => entity.Language).HasColumnName("language");
        }
    }
}
