﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.Context.Interfaces
{
    public interface IMappingInterface<T> where T : class
    {
        void MapEntity(EntityTypeBuilder<T> builder);
    }
}
