﻿using System;
using GameStore.Context.Interfaces;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameStore.Context
{
    public class OrderMapping: IMappingInterface<Order>
    {

        
        public void MapEntity(EntityTypeBuilder<Order> builder)
        {
            builder.ForNpgsqlToTable("order");

            builder.HasKey(entity => entity.OrderId);
            builder.HasMany(entity => entity.OrderGame);
            builder.HasOne(entity => entity.Customer)
                .WithMany(x => x.Orders)
                .HasForeignKey(key => key.CustomerId)
                .IsRequired();
            
            builder.Property(entity => entity.OrderId).HasColumnName("orderid");
            builder.Property(entity => entity.DateSale).HasColumnName("datesale");
            builder.Property(entity => entity.CustomerId).HasColumnName("customer");
            builder.Property(x => x.Status).HasColumnName("order_status");

        }
    }
}
