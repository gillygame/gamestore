﻿using System;
using GameStore.Context.Interfaces;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.Context.Mapping
{
    public class OrderGamesMapping : IMappingInterface<OrderGames>
    {
        public void MapEntity(EntityTypeBuilder<OrderGames> builder)
        {
            builder.ForNpgsqlToTable("order_games");
            builder.HasKey(entity => entity.OrderGamesId);
            builder.HasOne(entity => entity.Order).WithMany(x=>x.OrderGame);
            builder.HasOne(entity => entity.Game).WithMany(x=>x.OrderGame);

            builder.Property(entity => entity.OrderGamesId).HasColumnName("order_gamesid");
            builder.Property(entity => entity.OrderId).HasColumnName("order");
            builder.Property(entity => entity.GameId).HasColumnName("game");
        }
    }
}
