﻿using System;
using GameStore.Context.Interfaces;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace GameStore.Context
{
    public class GameMapping:IMappingInterface<Game>
    {
      

        public void MapEntity(EntityTypeBuilder<Game> builder)
        {
            builder.ForNpgsqlToTable("games");
            builder.HasKey(entity => entity.GameId);
            builder.HasMany(entity => entity.OrderGame);
            builder.HasOne(entity => entity.Category).WithMany(x => x.Games);

            builder.Property(entity => entity.GameId).HasColumnName("gameid");
            builder.Property(entity => entity.CategoryId).HasColumnName("category");
            builder.Property(entity => entity.Name).HasColumnName("name");
            builder.Property(entity => entity.Description).HasColumnName("description");
            builder.Property(entity => entity.Price).HasColumnName("price");
            builder.Property(entity => entity.Photo).HasColumnName("image");
        }
    }
}
