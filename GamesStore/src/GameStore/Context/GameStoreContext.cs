﻿using GameStore.Authorization.Entities.Identity;
using GameStore.Context.Mapping;
using GameStore.Entities;
using Microsoft.EntityFrameworkCore;

namespace GameStore.Context
{
    internal class GameStoreContext : DbContext
    {


        public GameStoreContext(DbContextOptions<GameStoreContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            new CategoryMapping().MapEntity(builder.Entity<Category>());
            new GameMapping().MapEntity(builder.Entity<Game>());
            new CustomerMapping().MapEntity(builder.Entity<Customer>());
            new OrderMapping().MapEntity(builder.Entity<Order>());
            new OrderGamesMapping().MapEntity(builder.Entity<OrderGames>());
            new RateMapping().MapEntity(builder.Entity<Rate>());

            new IdentityUserClaimMapping().MapEntity(builder.Entity<IdentityUserClaim>());
            new IdentityRoleClaimMapping().MapEntity(builder.Entity<IdentityRoleClaim>());
            new IdentityRoleMapping().MapEntity(builder.Entity<IdentityRole>());
            new IdentityUserLoginMapping().MapEntity(builder.Entity<IdentityUserLogin>());
            new IdentityUserMapping().MapEntity(builder.Entity<IdentityUser>());
            new IdentityUserRoleMapping().MapEntity(builder.Entity<IdentityUserRole>());


            base.OnModelCreating(builder);
        }

    }
}
