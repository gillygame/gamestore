﻿using Autofac;
using GameStore.Context;

namespace GameStore.Authorization.AspNet.Identity.Modules
{
    public class IdentityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<GameStoreContext>();
        }
    }
}
