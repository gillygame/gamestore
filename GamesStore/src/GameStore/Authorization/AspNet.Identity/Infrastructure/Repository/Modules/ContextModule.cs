﻿using Autofac;
using GameStore.Context;
using Microsoft.EntityFrameworkCore;

namespace GameStore.Authorization.AspNet.Identity.Infrastructure.Repository.Modules
{
    public class ContextModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<GameStoreContext>()
                .As<DbContext>()
                .InstancePerLifetimeScope();

        }
    }
}
