﻿using Autofac;
using GameStore.Authorization.AspNet.Identity.Infrastructure.Repository.Identity;
using GameStore.Authorization.AspNet.Identity.Infrastructure.Repository.Identity.Interfaces;

namespace GameStore.Authorization.AspNet.Identity.Infrastructure.Repository.Modules
{
    public class IdentityModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<UserReadRepository>().As<IUserReadRepository>();
        }
    }
}
