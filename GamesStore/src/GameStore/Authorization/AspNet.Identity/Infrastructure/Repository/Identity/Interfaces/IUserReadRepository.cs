﻿using System.Threading.Tasks;
using GameStore.Authorization.Entities.Identity;

namespace GameStore.Authorization.AspNet.Identity.Infrastructure.Repository.Identity.Interfaces
{
    public interface IUserReadRepository
    {
        Task<IdentityUser[]> SearchUsersAsync(string query, int? limit);
        Task<IdentityUser[]> GetAllUsers(int? limit);
        Task<IdentityUser> GetUserAsync(long id);
    }
}
