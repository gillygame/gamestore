﻿namespace GameStore.Authorization.Entities.Identity
{
    public class IdentityUserRole
    {
        public long UserId { get; set; }
        public long RoleId { get; set; }
    }
}
