﻿using GameStore.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using GameStore.Request;
using GameStore.Repository.GameRepositories.Interfaces;
using GameStore.Entities;
using GameStore.Repository.CategoryRepositories.Interfaces;
using GameStore.Response;

namespace GameStore.Services
{
    public class GameService : IGameService
    {
        private readonly IGameWriteRepository gameWriteRepository;
        private readonly IGameReadRepository gameReadRepository;
        private readonly ICategoryReadRepository categoryReadRepository;

        public GameService(IGameWriteRepository gameWriteRepository,
            ICategoryReadRepository categoryReadRepository,
            IGameReadRepository gameReadRepository)
        {
            this.gameWriteRepository = gameWriteRepository;
            this.categoryReadRepository = categoryReadRepository;
            this.gameReadRepository = gameReadRepository;
        }

        public async Task AddGameAsync(GameRequest request)
        {

            var categoryId = categoryReadRepository.GetCategoryByName(request.Category);
            var game = new Game
            {
                Name = request.Name,
                Price = request.Price,
                CategoryId = categoryId,
                Description = request.Description,
                Photo = request.Image
            };
            await gameWriteRepository.AddAsync(game);


        }

        public async Task<GameResponse[]> GetGameByCategoryAsync(string category)
        {
            var temp = await gameReadRepository.GetGameByCategoryAsync(category);

            var gameReponse = temp.Select(Game).ToArray();
            return gameReponse;
        }

        private static GameResponse Game(Game x)
        {
            return new GameResponse
            {
                Category = x.Category.Name,
                Description = x.Description,
                Name = x.Name,
                CategoryId = x.CategoryId.ToString(),
                GameId = x.GameId.ToString(),
                Photo = x.Photo,
                Price = x.Price
            };
        }

        public async Task<GameResponse[]> GetGameByNameAsync(string name)
        {
            var temp = await gameReadRepository.GetGameByNameAsync(name);

            var gameReponse = temp.Select(Game).ToArray();
            return gameReponse;
        }

        public async Task<GameResponse[]> GetGameAsync()
        {
            var temp = await gameReadRepository.GetAllGameAsync();

            var gameReponse = temp.Select(Game).ToArray();
            return gameReponse;
        }

        public async Task<GameResponse> GetGameByIdAsync(int gameId)
        {
            var game = await gameReadRepository.GetGameByIdAsync(gameId);

            var gameResponse = new GameResponse
            {
                Category = game.Category.Name,
                Description = game.Description,
                Name = game.Name,
                GameId = game.GameId.ToString(),
                Photo = game.Photo,
                Price = game.Price
            };
            return gameResponse;
        }

        public async Task EditGameAsync(GameRequest request)
        {
            var game = new Game
            {
                Description = request.Description,
                Name = request.Name,
                Price = request.Price,
                GameId = Convert.ToInt32(request.Category)
            };

            await gameWriteRepository.EditAsync(game);

        }
    }
}
