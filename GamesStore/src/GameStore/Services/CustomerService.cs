﻿using GameStore.Services.Interfaces;
using System;
using System.Threading.Tasks;
using GameStore.Entities;
using Microsoft.AspNetCore.Identity;
using GameStore.Authorization.Entities.Identity;
using GameStore.Repository.CustomerRepositories.Interfaces;
using GameStore.Request;

namespace GameStore.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly ICustomerWriteRepository customerWriteRepository;

        public CustomerService(UserManager<IdentityUser> userManagers, ICustomerWriteRepository groupReadRepositorys)
        {
            userManager = userManagers;
            customerWriteRepository = groupReadRepositorys;
        }

        public Task AddCustomerAsync(AccountRequest request)
        {
            return Task.Run(async () =>
            {
                var customer = new Customer
                {
                    FullName = request.FullName,
                    Country = request.Country,
                    DateBirthsday = request.DateBirthsday,
                    Email = request.Email,
                    DateRegistration = DateTime.Now.ToShortDateString(),
                    Language = request.Language,
                    Password = request.Password
                };
                await customerWriteRepository.AddAsync(customer);
                var user = new IdentityUser
                {
                    UserName = request.Email.Split('@')[0],
                    Email = request.Email
                };
                await userManager.CreateAsync(user, request.Password);
                await userManager.AddToRoleAsync(user, "Customer");
            }
            );
        }
    }
}
