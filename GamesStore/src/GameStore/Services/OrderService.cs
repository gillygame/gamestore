﻿using GameStore.Services.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using GameStore.Request;
using GameStore.Repository.OrderRepositories.Interfaces;
using GameStore.Repository.CustomerRepositories.Interfaces;
using GameStore.Entities;
using GameStore.Response;
using System.Collections.Generic;

namespace GameStore.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderWriteRepository orderWriteRepository;
        private readonly IOrderReadRepository orderReadRepository;
        private readonly ICustomerReadRepository customerReadRepository;

        public OrderService(IOrderWriteRepository orderWrite, ICustomerReadRepository customerRead,
            IOrderReadRepository orderRead)
        {
            this.orderWriteRepository = orderWrite;
            this.customerReadRepository = customerRead;
            this.orderReadRepository = orderRead;
        }
        public async Task AddOrderAsync(OrderRequest request)
        {
            var customer = await customerReadRepository.GetUserByLoginAsync(request.Customer);
            var countItem = await orderReadRepository.GetLastUnCompleatCustomerOrderAsync(customer.CustomerID);
            if (countItem == null)
            {
                var order = new Order
                {
                    Customer = customer,
                    DateSale = DateTime.Now.ToShortDateString(),
                    Status = false
                };
                await orderWriteRepository.AddOrderAsync(order);
            }
        }

        public Task AddGameToBucketAsync(OrderGameRequest request)
        {
            return Task.Run(async () =>
            {
                var customer = await customerReadRepository.GetUserByLoginAsync(request.Customer);
                var order = await orderReadRepository.GetLastUnCompleatCustomerOrderAsync(customer.CustomerID);
                if(order.OrderGame.FirstOrDefault(x=> x.GameId == request.GameId) != null)
                {
                    return;
                }
                var gameOrder = new OrderGames
                {
                    GameId = request.GameId,
                    OrderId = order.OrderId
                };
                await orderWriteRepository.AddGameToBucketAsync(gameOrder);
            });
        }

        public async Task<GameResponse[]> GetGameFromBucketAsync(OrderRequest order)
        {
            var customer = await customerReadRepository.GetUserByLoginAsync(order.Customer);
            var orderId = await orderReadRepository.GetLastUnCompleatCustomerOrderAsync(customer.CustomerID);

            var temp = await orderReadRepository.GetGameFromBucketAsync(orderId);

            return temp.Select(GameResponse).ToArray();
        }

        public async Task<GameResponse[]> GetCustomerGameAsync(OrderRequest order)
        {
            var customer = await customerReadRepository.GetUserByLoginAsync(order.Customer);

            var temp = await orderReadRepository.GetCustomerGameAsync(customer.CustomerID);

            return temp.Select(GameResponse).ToArray();
        }

        public async Task<OrderResponse[]> GetCustomerOrderAsync(OrderRequest order)
        {
            var customer = await customerReadRepository.GetUserByLoginAsync(order.Customer);

            var orders = await orderReadRepository.GetCustomerOrderAsync(customer.CustomerID);

            var oderesCustomer = orders.Select(CreateOrderResponse).ToArray();

            foreach (var item in oderesCustomer)
            {
                var gamesCollection = await orderReadRepository.GetGameFromOrderAsync(item.OrderId);
                item.Games = gamesCollection.Select(GameResponse).ToArray();
            }

            return oderesCustomer;
        }

        private static OrderResponse CreateOrderResponse(Order x)
        {
            return new OrderResponse
            {
                Customer = x.Customer.FullName,
                Date = x.DateSale,
                OrderId = x.OrderId,

            };
        }

        private static GameResponse GameResponse(OrderGames x)
        {
            return new GameResponse
            {
                GameId = x.Game.GameId.ToString(),
                Category = x.Game.Category.Name,
                Name = x.Game.Name,
                Description = x.Game.Description,
                Photo = x.Game.Photo,
                Price = x.Game.Price
            };
        }

        public async Task<OrderResponse[]> GetAllOrderAsync()
        {
            var orders = (await orderReadRepository.GetAllOrederAsync())
                .Select(x => new OrderResponse
                {
                    Customer = x.Customer.FullName,
                    Date = x.DateSale,
                    OrderId = x.OrderId,
                }).ToArray();

            foreach (var item in orders)
            {
                item.Games = (await orderReadRepository.GetGameFromOrderAsync(item.OrderId))
                    .Select(x => new GameResponse
                    {
                        GameId = x.Game.GameId.ToString(),
                        Category = x.Game.Category.Name,
                        Name = x.Game.Name,
                        Description = x.Game.Description,
                        Photo = x.Game.Photo,
                        Price = x.Game.Price
                    }).ToArray();
            }

            return orders;
        }

        public async Task DeleteGameFromOrderAsync(DeleteGameRequest request)
        {
            var orderGame = new OrderGames
            {
                GameId = request.GameId,
                OrderId = request.OrderId
            };
            await orderWriteRepository.DeleteFromBucketAsync(orderGame);
        }

        public Task DeleteGameFromBucketAsync(OrderGameRequest request)
        {
            return Task.Run(async () =>
            {
                var customer = await customerReadRepository.GetUserByLoginAsync(request.Customer);
                var orderId = await orderReadRepository.GetLastUnCompleatCustomerOrderAsync(customer.CustomerID);

                var orderGame = new OrderGames
                {
                    GameId = request.GameId,
                    OrderId = orderId.OrderId
                };
                await orderWriteRepository.DeleteFromBucketAsync(orderGame);
            });
        }

        public Task UpdateOrderAsync(OrderRequest request)
        {
            return Task.Run(async () =>
            {
                var customer = await customerReadRepository.GetUserByLoginAsync(request.Customer);
                var countItem = await orderReadRepository.GetLastUnCompleatCustomerOrderAsync(customer.CustomerID);

                await orderWriteRepository.UpdateOrderAsync(countItem);
            });
        }

        public async Task DeleteOrderAsync(int orderId)
        {
            var order = new Order { OrderId = orderId };
            await orderWriteRepository.DeleteOrderAsync(order);
        }

        public async Task<List<int[]>> GetAllTransaction()
        {
            var orders = await orderReadRepository.GetAllOrder();
            return orders
                  .Select(x => x.OrderGame
                  .Select(a => a.GameId).ToArray())
                  .ToList();
        }
    }
}
