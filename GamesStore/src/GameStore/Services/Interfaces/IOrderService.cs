﻿using GameStore.Request;
using GameStore.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.Interfaces
{
    public interface IOrderService
    {
        Task AddOrderAsync(OrderRequest request);
        Task UpdateOrderAsync(OrderRequest request);
        Task DeleteOrderAsync(int orderId);
        Task AddGameToBucketAsync(OrderGameRequest request);
        Task DeleteGameFromBucketAsync(OrderGameRequest request);
        Task DeleteGameFromOrderAsync(DeleteGameRequest request);
        Task<GameResponse[]> GetGameFromBucketAsync(OrderRequest order);
        Task<GameResponse[]> GetCustomerGameAsync(OrderRequest order);
        Task<OrderResponse[]> GetCustomerOrderAsync(OrderRequest order);
        Task<OrderResponse[]> GetAllOrderAsync();
        Task<List<int[]>> GetAllTransaction();
    }
}
