﻿using GameStore.Request;
using GameStore.Response;
using System.Threading.Tasks;

namespace GameStore.Services.Interfaces
{
    public interface IRateServices
    {
        Task AddFeedAsync(RateRequest rate);
        Task<RateResponse[]> GetGameFeedsAsync(int gameId);
        Task<GameResponse[]> GetMostSaleGame();
        Task<GameResponse[]> GetMostSaleGameByCategory(string name);
        Task<GameResponse[]> GetMostRateGame();
    }
}
