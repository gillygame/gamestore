﻿using GameStore.Entities;
using GameStore.Request;
using GameStore.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.Interfaces
{
    public interface ICustomerService
    {
        Task AddCustomerAsync(AccountRequest request);
    }
}
