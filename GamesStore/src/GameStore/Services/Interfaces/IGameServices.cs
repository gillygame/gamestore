﻿using GameStore.Request;
using GameStore.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Services.Interfaces
{
    public interface IGameService
    {
        Task<GameResponse[]> GetGameByCategoryAsync(string category);
        Task<GameResponse[]> GetGameByNameAsync(string name);
        Task<GameResponse[]> GetGameAsync();
        Task<GameResponse> GetGameByIdAsync(int gameId);

        Task AddGameAsync(GameRequest request);
        Task EditGameAsync(GameRequest request);
    }
}
