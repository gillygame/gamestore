﻿using GameStore.Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using GameStore.Request;
using GameStore.Response;
using GameStore.Repository.RateRepository.Interfaces;
using GameStore.Repository.CustomerRepositories.Interfaces;
using GameStore.Entities;
using System;
using GameStore.Repository.CategoryRepositories.Interfaces;
using GameStore.Repository.GameRepositories.Interfaces;
using System.Collections.Generic;

namespace GameStore.Services
{
    public class RateServices : IRateServices
    {
        private readonly IRateWriteRepository rateWriteRepository;
        private readonly IRateReadRepository rateReadRepository;
        private readonly IGameReadRepository gameReadRepository;
        private readonly ICustomerReadRepository customerReadRepository;
        private readonly ICategoryReadRepository categoryReadRepository;


        public RateServices(
            IRateWriteRepository rateWriteRepository,
            ICustomerReadRepository customerRead,
            IRateReadRepository rateReadRepository,
            ICategoryReadRepository categoryReadRepository,
            IGameReadRepository gameReadRepository)
        {
            this.rateWriteRepository = rateWriteRepository;
            this.customerReadRepository = customerRead;
            this.gameReadRepository = gameReadRepository;
            this.rateReadRepository = rateReadRepository;
            this.categoryReadRepository = categoryReadRepository;
        }

        public async Task AddFeedAsync(RateRequest rate)
        {
            var customer = await customerReadRepository.GetUserByLoginAsync(rate.Customer);

            var entityRate = new Rate
            {
                CustomerId = customer.CustomerID,
                GameId = rate.GameId,
                Comment = rate.Comment,
                CustomerRate = rate.CustomerRate
            };
            await rateWriteRepository.AddFeedAsync(entityRate);
        }

        public async Task<RateResponse[]> GetGameFeedsAsync(int gameId)
        {
            var gameRates = await rateReadRepository.GetGameRates(gameId);

            return gameRates.Select(CreateRateResponse).ToArray();
        }

        private static RateResponse CreateRateResponse(Rate x)
        {
            return new RateResponse
            {
                Customer = x.Customer.FullName,
                Comment = x.Comment,
                Mark = x.CustomerRate,
                RateId = x.RateId
            };
        }

        public async Task<GameResponse[]> GetMostSaleGame()
        {
            var gameList = await rateReadRepository.GetMostSaleGame();
            var gameResponseList = new List<GameResponse>();
            foreach (var item in gameList)
            {
                var game = await gameReadRepository.GetGameByIdAsync(item);

                var gameResponse = new GameResponse
                {
                    Category = game.Category.Name,
                    Description = game.Description,
                    Name = game.Name,
                    GameId = game.GameId.ToString(),
                    Photo = game.Photo,
                    Price = game.Price
                };
                gameResponseList.Add(gameResponse);
            }
            return gameResponseList.ToArray();
        }

        public async Task<GameResponse[]> GetMostSaleGameByCategory(string name)
        {
            var gameList = await rateReadRepository
                .GetMostSaleGameByCategory(categoryReadRepository.GetCategoryByName(name));

            var gameResponseList = new List<GameResponse>();
            foreach (var item in gameList)
            {
                var game = await gameReadRepository.GetGameByIdAsync(item);

                var gameResponse = new GameResponse
                {
                    Category = game.Category.Name,
                    Description = game.Description,
                    Name = game.Name,
                    GameId = game.GameId.ToString(),
                    Photo = game.Photo,
                    Price = game.Price
                };
                gameResponseList.Add(gameResponse);
            }
            return gameResponseList.ToArray();
        }

        public async Task<GameResponse[]> GetMostRateGame()
        {
            var gameList = await rateReadRepository
                .GetMostRateGame();

            var gameResponseList = new List<GameResponse>();
            foreach (var item in gameList)
            {
                var game = await gameReadRepository.GetGameByIdAsync(item);

                var gameResponse = new GameResponse
                {
                    Category = game.Category.Name,
                    Description = game.Description,
                    Name = game.Name,
                    GameId = game.GameId.ToString(),
                    Photo = game.Photo,
                    Price = game.Price
                };
                gameResponseList.Add(gameResponse);
            }
            return gameResponseList.ToArray();
        }
    }


}

