﻿using Autofac;
using GameStore.Authorization.AspNet.Identity.Infrastructure.Repository.Identity;
using GameStore.Authorization.AspNet.Identity.Infrastructure.Repository.Identity.Interfaces;
using GameStore.Context;
using GameStore.Repository.CategoryRepositories;
using GameStore.Repository.CategoryRepositories.Interfaces;
using GameStore.Repository.CustomerRepositories;
using GameStore.Repository.CustomerRepositories.Interfaces;
using GameStore.Repository.GameRepositories;
using GameStore.Repository.GameRepositories.Interfaces;
using GameStore.Repository.OrderRepositories;
using GameStore.Repository.OrderRepositories.Interfaces;
using GameStore.Repository.RateRepository;
using GameStore.Repository.RateRepository.Interfaces;
using GameStore.Services;
using GameStore.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
namespace GameStore.Modules
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
               .RegisterType<GameStoreContext>()
               .As<DbContext>()
               .InstancePerLifetimeScope();

            builder.RegisterType<CategoryReadRepository>().As<ICategoryReadRepository>();
            builder.RegisterType<CustomerReadRepository>().As<ICustomerReadRepository>();
            builder.RegisterType<CustomerWriteRepository>().As<ICustomerWriteRepository>();

            builder.RegisterType<GameReadRepository>().As<IGameReadRepository>();
            builder.RegisterType<GameWriteRepository>().As<IGameWriteRepository>();
            builder.RegisterType<OrderReadRepository>().As<IOrderReadRepository>();
            builder.RegisterType<OrderWriteRepository>().As<IOrderWriteRepository>();
            builder.RegisterType<RateReadRepository>().As<IRateReadRepository>();
            builder.RegisterType<RateWriteRepository>().As<IRateWriteRepository>();
            builder.RegisterType<UserReadRepository>().As<IUserReadRepository>();

            builder.RegisterType<CustomerService>().As<ICustomerService>();
            builder.RegisterType<GameService>().As<IGameService>();
            builder.RegisterType<OrderService>().As<IOrderService>();
            builder.RegisterType<RateServices>().As<IRateServices>();
        }
    }
}
