﻿using Autofac;
using GameStore.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace GameStore.Modules
{
    public class DatabaseModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(CreateDbContextOptions).As<DbContextOptions<GameStoreContext>>();
        }

        private static DbContextOptions<GameStoreContext> CreateDbContextOptions(IComponentContext context)
        {
            var configuration = context.Resolve<IConfiguration>();
            string connectionString = configuration["Data:DefaultConnection:ConnectionString"];
            var dbContextOptionsBuilder = new DbContextOptionsBuilder<GameStoreContext>();
            dbContextOptionsBuilder.UseNpgsql(connectionString);

            return dbContextOptionsBuilder.Options;
        }
        
    }
}
