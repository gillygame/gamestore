﻿using Autofac;
using GameStore.Infrastructure.Wrappers;

namespace GameStore.Modules
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SignInManegerWrapper>();
        }
    }
}
    