﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GameStore.Entities
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string DateBirthsday { get; set; }
        public string DateRegistration { get; set; }
        public string Country { get; set; }
        public string Language { get; set; }

        public virtual ICollection<Rate> UsersRate { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

        public Customer()
        {
            Orders = new List<Order>();
        }

        public Customer(Customer customer)
        {
            FullName = customer.FullName;
            Email = customer.Email;
            Password = customer.Password;
            DateBirthsday = customer.DateBirthsday;
            DateRegistration = customer.DateRegistration;
            Country = customer.Country;
            Language = customer.Language;

            Orders = new List<Order>();

        }
    }
}
