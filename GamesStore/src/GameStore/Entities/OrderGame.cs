﻿namespace GameStore.Entities
{
    public class OrderGames
    {
        public int OrderGamesId { get; set; }
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }
        public int GameId { get; set; }
        public virtual Game Game { get; set; }

        public OrderGames()
        {

        }

        public OrderGames(int orderGameId, int orderId, Order order, int gameId, Game game)
        {
            OrderId = orderId;
            Order = order;
            GameId = gameId;
            Game = game;
        }
    }
}
