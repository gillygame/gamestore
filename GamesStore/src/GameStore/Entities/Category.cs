﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace GameStore.Entities
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Game> Games { get; set; }

        public Category()
        {
           Games = new List<Game>();
        }
        public Category(string name)
        {
            Games = new List<Game>();
            this.Name = name;
        }
    }
}
