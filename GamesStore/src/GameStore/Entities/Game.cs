﻿using System.Collections.Generic;

namespace GameStore.Entities
{
    public class Game
    {
        public int GameId { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public string Photo { get; set; }

        public virtual ICollection<OrderGames> OrderGame { get; set; }
        public virtual ICollection<Rate> GamesRate { get; set; }

        public Game(Game game)
        {
            Name = game.Name;
            CategoryId = game.CategoryId;
            Description = game.Description;
            Price = game.Price;
            Photo = game.Photo;

            OrderGame = new List<OrderGames>();
        }

        public Game()
        {
            OrderGame = new List<OrderGames>();
        }

    }
}
