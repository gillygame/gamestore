﻿namespace GameStore.Entities
{
    public class Rate
    {
        public int RateId { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public int GameId { get; set; }
        public virtual Game Game { get; set; }
        public string Comment { get; set; }
        public double CustomerRate { get; set; }

        public Rate()
        {

        }

        public Rate(int customerId, Customer customer, int gameId,
            Game game,string comment, double customerRate)
        {
            CustomerId = customerId;
            Customer = customer;
            GameId = gameId;
            Game = game;
            Comment = comment;
            CustomerRate = customerRate;
        }
    }
}
