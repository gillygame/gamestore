﻿using System.Collections.Generic;

namespace GameStore.Entities
{
    public class Order
    {
        public int OrderId { get; set; }
        public virtual Customer Customer { get; set; }
        public string DateSale { get; set; }
        public bool Status { get; set; }
        public virtual ICollection<OrderGames> OrderGame { get; set; }
        public virtual int CustomerId { get; set; }

        public Order()
        {
            OrderGame = new List<OrderGames>();
        }

        public Order(int orderId, bool orderStatus,  Customer customer, string dateSale)
        {
            OrderId = orderId;
            Status = orderStatus;
            Customer = customer;
            DateSale = dateSale;

            OrderGame = new List<OrderGames>();

        }
    }
}
