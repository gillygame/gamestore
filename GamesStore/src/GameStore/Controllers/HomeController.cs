﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GameStore.Repository.CategoryRepositories.Interfaces;

namespace GameStore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICategoryReadRepository categoryReadRepository;
        public HomeController(ICategoryReadRepository repository)
        {
            this.categoryReadRepository = repository;
        }

        public async Task<IActionResult> Index()
        {
            var item = await categoryReadRepository.GetAllCategoryAsync();
            return View();
        }
    }
}
