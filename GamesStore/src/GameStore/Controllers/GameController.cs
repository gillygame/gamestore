﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.IO;
using GameStore.Request;
using System;
using GameStore.Response;

namespace GameStore.Controllers
{
    public class GameController : Controller
    {
        private readonly IHostingEnvironment _environment;

        public GameController(IHostingEnvironment environment)
        {
            _environment = environment;
        }

        [HttpGet]
        public IActionResult AddGame()
        {
            if (ViewBag.Game == null)
            {
                ViewBag.Game = new GameRequest() { Price = 0,Name=" ",Category = string.Empty,Description =" ",Image = " "};
                

            }
            return View();
        }

        [HttpGet]
        public IActionResult Order()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GameProfile()
        {
            var id = Convert.ToInt32(Request.Query["gameId"]);
            var gameResponse = new CurrentGameResponse
            {
                GameId = id
            };
            return View(gameResponse);
        }

        [HttpGet]
        public IActionResult OwnGameList()
        {
            return View();
        }

        [HttpGet]
        public IActionResult EditOrDeleteGame()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ShowGame()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Bucket()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddGame(ICollection<IFormFile> files, string gameName, string gameDescription, string gamePrice)
        {
            GameRequest request = new GameRequest();
            request.Description = gameDescription;
            request.Name = gameName;
            request.Price = Convert.ToInt32(gamePrice);
            

            var uploads = _environment.WebRootPath;
            uploads += "\\images";

            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
                    {
                         request.Image = file.FileName;
                        await file.CopyToAsync(fileStream);
                    }
                }
            }
            ViewBag.Game = request;
            return View();
        }
    }
}
