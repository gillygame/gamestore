﻿using GameStore.Request;
using GameStore.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Web.Http;

namespace GameStore.Controllers.WebApi
{
    [Route("api/[controller]/[action]")]

    public class AccountController : ApiController
    {
        private readonly ICustomerService customerService;

        public AccountController(ICustomerService customerService)
                             
        {
         
            this.customerService = customerService;
        }

        [HttpPost]
        public async Task CreateCustomer([FromBody] AccountRequest customer)
        {
            await customerService.AddCustomerAsync(customer);
        }
    }
}
