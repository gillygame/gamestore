﻿using GameStore.Repository.GameRepositories.Interfaces;
using GameStore.Request;
using GameStore.Response;
using GameStore.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using RecomendationService.Recomendation;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
namespace GameStore.Controllers.WebApi
{
    [Route("api/[controller]/[action]")]
    public class OrderController : ApiController
    {
        private readonly IGameService gameService;
        private readonly IGameReadRepository gameReadRepository;
        private readonly IGameWriteRepository gameWriteRepository;
        private readonly IOrderService orderService;

        public OrderController(
            IGameService gameService,
            IGameReadRepository gameList,
            IOrderService category,
            IGameWriteRepository gameWrite)
        {
            this.gameService = gameService;
            this.gameReadRepository = gameList;
            this.orderService = category;
            this.gameWriteRepository = gameWrite;
        }

        [HttpPost]
        public async Task CreateOrder([FromBody] OrderRequest order)
        {
            await orderService.AddOrderAsync(order);
        }

        [HttpPost]
        public async Task ConfirmOrder([FromBody] OrderRequest order)
        {
            await orderService.UpdateOrderAsync(order);
        }

        [HttpPost]
        public async Task AddToBucket([FromBody] OrderGameRequest order)
        {
            await orderService.AddGameToBucketAsync(order);
        }

        [HttpPost]
        public async Task DeleteGameFromBucket([FromBody] OrderGameRequest order)
        {
            await orderService.DeleteGameFromBucketAsync(order);
        }

        [HttpPost]
        public async Task DeleteGameFromOrder([FromBody] DeleteGameRequest order)
        {
            await orderService.DeleteGameFromOrderAsync(order);
        }

        [HttpPost]
        public async Task DeleteOrder([FromBody] string orderId)
        {
            await orderService.DeleteOrderAsync(System.Convert.ToInt32(orderId));
        }

        [HttpGet]
        public async Task<GameResponse[]> GetGamesFromBucket(string customer)
        {
            return await orderService.GetGameFromBucketAsync(new OrderRequest { Customer = customer });
        }

        [HttpGet]
        public async Task<GameResponse[]> GetCusomerGame(string customer)
        {
            var orderRequest = CreateOrderRequest(customer);
            var temp = await orderService.GetCustomerGameAsync(orderRequest);
            return temp;
        }

        [HttpGet]
        public async Task<OrderResponse[]> GetCustomerOrder(string customer)
        {
            var orderRequest = CreateOrderRequest(customer);

            var temp = await orderService.GetCustomerOrderAsync(orderRequest);
            return temp;
        }

        [HttpGet]
        public async Task<OrderResponse[]> GetAllOrder()
        {
            var temp = await orderService.GetAllOrderAsync();
            return temp;
        }

        private static OrderRequest CreateOrderRequest(string customer)
        {
            return new OrderRequest { Customer = customer };
        }
    }
}
