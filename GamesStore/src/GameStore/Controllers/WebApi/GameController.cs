﻿using GameStore.Entities;
using GameStore.Repository.CategoryRepositories.Interfaces;
using GameStore.Repository.GameRepositories.Interfaces;
using GameStore.Request;
using GameStore.Response;
using GameStore.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace GameStore.Controllers.WebApi
{
    [Route("api/[controller]/[action]")]
    public class GameController : ApiController
    {
        private readonly IGameService gameService;
        private readonly IGameReadRepository gameReadRepository;
        private readonly IGameWriteRepository gameWriteRepository;
        private readonly ICategoryReadRepository categoryReadRepository;

        public GameController(
            IGameService gameService,
            IGameReadRepository gameList,
            ICategoryReadRepository category,
            IGameWriteRepository gameWrite)
        {
            this.gameService = gameService;
            this.gameReadRepository = gameList;
            this.categoryReadRepository = category;
            this.gameWriteRepository = gameWrite;
        }

        [HttpPost]
        public async Task CreateGame([FromBody] GameRequest game)
        {
            string photoPath = @"/images/" + game.Image;
            game.Name = game.Name.ToUpper();
            game.Image = photoPath;
            await gameService.AddGameAsync(game);
        }

        [HttpPost]
        public async Task DeleteGame([FromBody] string gameId)
        {
            await gameWriteRepository.DeleteAsync(Convert.ToInt32(gameId));
        }

        [HttpPost]
        public async Task EditGame([FromBody] GameRequest game)
        {
            await gameService.EditGameAsync(game);
        }

        [HttpGet]
        public async Task<GameResponse[]> GetGames()
        {
            return await gameService.GetGameAsync();
        }

        [HttpGet]
        public async Task<GameResponse> GetGameById(int gameId)
        {
            return await gameService.GetGameByIdAsync(gameId);
        }
        [HttpGet]
        public async Task<GameResponse[]> GetGamesByName(string name)
        {
            return await gameService.GetGameByNameAsync(name.ToUpper());
        }

        [HttpGet]
        public async Task<GameResponse[]> GetGamesByCategory(string category)
        {
            return await gameService.GetGameByCategoryAsync(category);
        }

    }
}
