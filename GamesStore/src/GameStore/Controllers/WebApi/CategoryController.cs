﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Web.Http;
using GameStore.Repository.CategoryRepositories.Interfaces;
using GameStore.Entities;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace GameStore.Controllers.WebApi
{
    [Route("api/[controller]/[action]")]

    public class CategoryController : ApiController
    {
        private readonly ICategoryReadRepository categoryReadRepository;

        public CategoryController(ICategoryReadRepository categoryReadRepository)
        {
            this.categoryReadRepository = categoryReadRepository;
        }

        [HttpGet]
        public async Task<Category[]> GetCategory()
        {
            return await categoryReadRepository.GetAllCategoryAsync();
        }
    }
}
