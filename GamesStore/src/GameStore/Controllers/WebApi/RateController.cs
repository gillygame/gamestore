﻿using GameStore.Repository.GameRepositories.Interfaces;
using GameStore.Request;
using GameStore.Response;
using GameStore.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using RecomendationService.Recomendation;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace GameStore.Controllers.WebApi
{
    [Route("api/[controller]/[action]")]
    public class RateController : ApiController
    {
        private readonly IRateServices rateService;
        private readonly IGameService gameService;
        private readonly IOrderService orderService;

        public RateController(IRateServices rateService, IGameService gameService, IOrderService orderService)
        {
            this.rateService = rateService;
            this.gameService = gameService;
            this.orderService = orderService;
        }

        [HttpGet]
        public async Task<RateResponse[]> GetGameRates(int gameId)
        {
           return await rateService.GetGameFeedsAsync(gameId);
        }

        [HttpGet]
        public async Task<GameResponse[]> GetTopSaleGame()
        { 
            return await rateService.GetMostSaleGame();
        }

        [HttpGet]
        public async Task<GameResponse[]> GetTopSaleGameByCategory(string category)
        {
            return await rateService.GetMostSaleGameByCategory(category);
        }

        [HttpGet]
        public async Task<GameResponse[]> GetMostRateGame()
        {
            return await rateService.GetMostRateGame();
        }

        [HttpPost]
        public async Task CreateFeed([FromBody] RateRequest rate)
        {
            await rateService.AddFeedAsync(rate);
        }

        [HttpGet]
        public async Task<GameResponse[]> GetAllTranscations(int gameId)
        {
            var transactions = await orderService.GetAllTransaction();
            GetRecomendation recomendation = new GetRecomendation();
            var recomendItems = recomendation.Recomendation(transactions, gameId);

            var gameRecomendationList = new List<GameResponse>();

            foreach (var item in recomendItems)
            {
                gameRecomendationList.Add(await gameService.GetGameByIdAsync(item));
            }

            return gameRecomendationList.ToArray();
        }

    }
}
